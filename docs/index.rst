.. SecKit SA IDM Common documentation master file, created by


================================================
Welcome to Splunk_TA_Splunk's documentation!
================================================

This add-on configures Splunk to better process its own logs using field best practices.

- Line Breaking and Event Breaking
- Avoid should line merge
- Configure time stamp parsing based on known formats

This add-on should be deployed to all Universal Forwarders as well as all Splunk Enterprise roles such as indexers, search heads, cluster master, license master, and heavy forwarder.

.. toctree::
   :maxdepth: 2
   :glob:
