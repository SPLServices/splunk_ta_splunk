
#Name of the app folder and App ID as published to Splunk Base
MAIN_APP      = Splunk_TA_splunk

#Name of the license file in the root of the repo
LICENSE_FILE  = license-eula.txt
LICENSE_URL   = https://www.splunk.com/en_us/legal/splunk-software-license-agreement.html

AUTHOR = Ryan Faircloth
COMPANY = Splunk, Inc.

MAIN_DESCRIPTION = Splunk Add-on for Splunk logs
MAIN_LABEL = Splunk TA for Splunk

SPLUNKBASE    = https://splunkbase.splunk.com/app/xxx/
REPOSITORY    = https://bitbucket.org/SPLServices/splunk_ta_splunk/
DOCSSITE      = https://splunk-ta-splunk.readthedocs.io
PROJECTSITE   = https://bitbucket.org/account/user/SPLServices/projects/SEC_TA

#Used by the Copy right tool to place the correct copy right on new files
COPYRIGHT_LICENSE ?= SPLUNK
COPYRIGHT_HOLDER ?= $(COMPANY)
COPYRIGHT_YEAR ?= 2019

define rst_prolog
.. |MAIN_LABEL| replace:: $(MAIN_LABEL)
.. |VERSION| replace:: $(VERSION)
.. |RELEASE| replace:: $(VERSION)$(PACKAGE_SLUG)
.. |LICENSE| replace:: $(COPYRIGHT_LICENSE)
.. _Repository: $(REPOSITORY)
.. _SPLUNKBASE: $(SPLUNKBASE)
endef
export rst_prolog
